# See Cafe Göppingen

Website for the coffee shop in Göppingen, Germany.

http://www.seecafe-goeppingen.de/

![Screenshot of seecafe-goeppingen.de](./seecafe.jpg)

## Getting Started

```
$ git clone https://github.com/simonh9134/see_cafe.git
  cd see_cafe
  npm install
  gulp dev
```

## Deployment

```
$ gulp build
//Move the dist folder to your FTP root.
```

## Built With

* [Gulp](https://gulpjs.com/)
* [Visual Studio Code](https://code.visualstudio.com/)
* [Atom](https://atom.io/)
* [Swiper](http://idangero.us/swiper/)

## Authors

* **Oliver Staudenmayer** - *Creative design* - [lucadesign.de](http://www.lucadesign.de/)
