const logo = document.querySelector(".logo");

const menuItems = document.querySelectorAll(".js-menu-item");
const menuItemWhite = "menu__item--white";
const menuItemSelected = "menu__item--selected";

const menuLine = document.querySelector(".line--menu");
const menuLineWhite = "line--menu--white"

const sections = document.querySelectorAll(".js-section");

function menuBlack() {
    logo.src = "images/logo_schwarz.png";
    menuItems.forEach((menuItem) => {
        menuItem.classList.remove(menuItemWhite);
    });
    menuLine.classList.remove(menuLineWhite);
}

function menuWhite() {
    logo.src = "images/logo_weiss.png";
    menuItems.forEach((menuItem) => {
        menuItem.classList.add(menuItemWhite);
    });
    menuLine.classList.add(menuLineWhite);
}

function toggleClass(elements, index, className) {
    elements.forEach((element) => {
        element.classList.remove(className);
    });
    elements[index].classList.add(className);
}

function checkPosition() {
    sections.forEach((element, index) => {
        let sectionPositionTop = element.getBoundingClientRect().top;
        let sectionPositionBottom = element.getBoundingClientRect().bottom;

        if (10 >= sectionPositionTop && sectionPositionBottom >= 10) {
            toggleClass(menuItems, index, menuItemSelected);

            //toggle of menu colour
            if (index == 0 && screen.width >= 1024) {
                menuWhite();
            } else {
                menuBlack();
            }
        }
    });
}

checkPosition();

window.onscroll = function () {
    checkPosition();
};